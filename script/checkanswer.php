<?php
session_start();

//error_reporting(E_ALL);
//ini_set('display_errors', '1');

$question = $_SESSION['question'];
$answers = $_SESSION['answers'];

$scoring = $_SESSION['scoring'];

$questionNumber = $_SESSION['question_number'];


$id = $question->id;
$quizz_id = $question->quiz_id;
$title = $question->title;
$text = $question->text;
$correct_answer = $question->correct_answer;
$seconds = $question->seconds;
$points = $question->points;

// what did the user answer?
$userAnswer = $_POST['answer'];


// get the Google response given the question
$useragent = "Opera/9.80 (J2ME/MIDP; Opera Mini/4.2.14912/870; U; id) Presto/2.4.15";
$ch = curl_init ("");
curl_setopt ($ch, CURLOPT_URL, "http://www.google.com/search?hl=en&tbo=d&site=&source=hp&q=".urlencode($text));
curl_setopt ($ch, CURLOPT_USERAGENT, $useragent); // set user agent
curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
//curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
$result = curl_exec ($ch);
curl_close($ch);
$cleanResult = strtolower($result);

// find Googles answer
$googleAnswer = 'a';
$winningValue = 0;
foreach($answers as $key => &$answer)
{
    $matchScore = matchScore($cleanResult, $answer['text']);

    if ($matchScore > $winningValue)
    {
        $googleAnswer = $key;
        $winningValue = $matchScore;
    }    
}
$googleText = $answers[$googleAnswer]['text'];


// update Google's score
if ($googleAnswer == $correct_answer)
{
    $googleCorrect = 1;
    $scoring['google']['correct']++;
}
else
{
    $scoring['google']['wrong']++;
    $googleCorrect = 0;
}

// update the user's score
if ($userAnswer == $correct_answer)
{
    $userCorrect = 1;
    $scoring['user']['correct']++;
}
else
{
    $scoring['user']['wrong']++;
    $userCorrect = 0;
}

$_SESSION['scoring'] = $scoring;


$response = array(
    'question_number' => $questionNumber,
    'user_correct' => $userCorrect,
    'google_correct' => $googleCorrect,
    'correct_text' => $answers[$correct_answer]['text'],
    'google_text' => $googleText,
    'google_score' => $scoring['google']['correct'],
    'user_score' => $scoring['user']['correct'],
    'user_wrong' => $scoring['user']['wrong'],
    'google_wrong' => $scoring['google']['wrong'],
    'last_question' => 0
);

if ($questionNumber == 10)
{
    $response['last_question'] = 1;
}

sleep(1);

echo json_encode($response);
die();


// get the score of some answer given some text (google results).
function matchScore($text, $answer)
{
//        return similar_text($answer, $text);
    $words = explode(' ', trim(strtolower($answer)));
    $count = 0;
    foreach($words as $word)
    {
        $count += substr_count($text, $word);
    }

    return $count/(count($words)*1.0);
}

?>
