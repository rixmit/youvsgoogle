<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');

//require_once 'google-api-php-client/src/Google_Client.php';

session_start();


$question = $_SESSION['question'];

$id = $question->id;
$quizz_id = $question->quiz_id;
$title = $question->title;
$text = $question->text;
$answer_a = $question->answer_a;
$answer_b = $question->answer_b;
$answer_c = $question->answer_c;
$answer_d = $question->answer_d;
$correct_answer = $question->correct_answer;
$seconds = $question->seconds;
$points = $question->points;


$url = 'https://www.google.nl/search?q='.urlencode($text);
$html = file_get_contents($url);
$clean = strip_tags($html);

echo "A: ".similar_text($clean, $answer_a)."<br />";
echo "B: ".similar_text($clean, $answer_b)."<br />";
echo "C: ".similar_text($clean, $answer_c)."<br />";
echo "D: ".similar_text($clean, $answer_d)."<br />";
echo "Correct: ".$correct_answer;

?>
