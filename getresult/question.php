<?php
session_start();

error_reporting(E_ALL);
ini_set('display_errors', '1');

$correctAnswers = 0;
for ($idx=0; $idx < 1; $idx++)
{

    $limit = 10000;
    $page = 0;

    while (true)
    {
        // get some the quizzes
        $uri = "http://tools.ncrv.nl/nnqapi/index.php/quizzes/$limit/$page";
        $file = file_get_contents($uri);
        $json = json_decode($file); //print_r($json);
        
        if (count($json->result) == 0)
        {
            continue;
        }
        
        // pick a random quiz
        $n = rand(0, count($json->result)-1);
        $quiz = $json->result[$n];

        // pick a rando question
        $jsonq = json_decode(file_get_contents("http://tools.ncrv.nl/nnqapi/index.php/quiz/$quiz->id"));


        if (count($jsonq->result->questions) == 0)
        {
            continue;
        }
        $n = rand(0, count($jsonq->result->questions)-1);

        $question = $jsonq->result->questions[$n];
        
        break;
        
    }

    $id = $question->id;
    $quizz_id = $question->quiz_id;
    $title = $question->title;
    $text = $question->text;
    $answer_a = $question->answer_a;
    $answer_b = $question->answer_b;
    $answer_c = $question->answer_c;
    $answer_d = $question->answer_d;
    $correct_answer = $question->correct_answer;
    $seconds = $question->seconds;
    $points = $question->points;



//    $url =  "http://ajax.googleapis.com/ajax/services/search/web?v=1.0&key=AIzaSyBacVRiPNo7uMqhtjXG4Zeq1DtSQA_UOD4&cx=014517126046550339258:qoem7fagpyk&num=100&q=".urlencode($text);
//    $ch = curl_init();
//    curl_setopt($ch, CURLOPT_URL, $url);
//    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
//    curl_setopt($ch, CURLOPT_REFERER, 'http://www.google.com');
//    $body = curl_exec($ch);
//    curl_close($ch);
//
//    $resultJson = json_decode($body,true);
//
//    $clean = '';
//    foreach ($resultJson['responseData']['results'] as $data)
//    {
//        $clean = $clean.$data['title'].$data['content'];
//    }
//    
//    $clean = strtolower($clean);
//    
//    echo $clean."<br /><br /><br /><br />";



//    $url =  "https://www.google.nl/search?q=".urlencode($text);
//    echo $url;
//    $ch = curl_init();
//    curl_setopt($ch, CURLOPT_URL, $url);
//    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
//    $body = curl_exec($ch);
//    curl_close($ch);
//    
//    echo strlen($clean);
        
    
//    $url = 'https://www.google.nl/search?q='.urlencode($text);
//    $url = "http://ajax.googleapis.com/ajax/services/search/web?v=1.0&key=AIzaSyBacVRiPNo7uMqhtjXG4Zeq1DtSQA_UOD4&cx=014517126046550339258:qoem7fagpyk&num=100&q=".urlencode($text);
//    $html = file_get_contents($url);
//    echo strlen($html);
//    $clean = $html;
    

    $useragent = "Opera/9.80 (J2ME/MIDP; Opera Mini/4.2.14912/870; U; id) Presto/2.4.15";
    $ch = curl_init ("");
    curl_setopt ($ch, CURLOPT_URL, "http://www.google.com/search?hl=en&tbo=d&site=&source=hp&q=".urlencode($text));
    curl_setopt ($ch, CURLOPT_USERAGENT, $useragent); // set user agent
    curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
    $clean = curl_exec ($ch);
    curl_close($ch);
    echo strlen($clean);
    echo $clean;

//    $clean = strip_tags($html);

    //$clean = substr($clean, 0, 255);
    $clean = strtolower($clean);

    $answers = array(
        array('option' => 'a', 'text' => $answer_a),
        array('option' => 'b', 'text' => $answer_b),
        array('option' => 'c', 'text' => $answer_c),
        array('option' => 'd', 'text' => $answer_d)
    );


    echo $text."<br />";
    foreach($answers as &$answer)
    {
        $answer['text'] = strtolower($answer['text']);
        $answer['match'] = match($clean, $answer['text']);
        if ($answer['option'] == $correct_answer) { echo "<strong>"; }
        echo $answer['option'].". ".$answer['text'].": ".$answer['match'];
        if ($answer['option'] == $correct_answer) { echo "</strong>"; }
        echo "<br />";
    }

    if (winner($answers) == $correct_answer)
    {
        $correctAnswers = $correctAnswers + 1;
        echo "GOED!";
    }


}

echo $correctAnswers;


    function match($text, $answer)
    {
//        return similar_text($answer, $text);
        $words = explode(' ', trim($answer));
        $count = 0;
        foreach($words as $word)
        {
            $count += substr_count($text, $word);
        }
        
        return $count/(count($words)*1.0);
    }

    function winner($answers)
    {
        $winningOption = 'x';
        $winningValue = 0;

        foreach ($answers as $answer)
        {
            if ($answer['match'] > $winningValue)
            {
                $winningValue = $answer['match'];
                $winningOption = $answer['option'];
            }

        }

        return $winningOption;
    }

?>
