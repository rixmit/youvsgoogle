var containerHeight;
var windowHeight;

$(document).ready(function() {
	repositionContainer();
	$(window).resize(repositionContainer);
});

var repositionContainer = function() {

	windowHeight = $(window).height();
	containerHeight = $('.container').height();

	$('.container').css('margin-top', (windowHeight - containerHeight) / 5);

}


$('form#answer').on('change', function() {
	$('form#answer input[type="submit"]').show();
});


$('form#answer').on('submit', function(event) {

	event.preventDefault();
    
    var answer = $(this).find('input[name="answer"]:checked').val();

	$('.answers').stop(true,true).slideUp(350, function() {
		$('.loading').slideDown(350);
	});

	$.ajax({

		url: '/script/checkanswer.php',
		type: 'post',
		dataType: 'json',
		data: {
			questionId: 1,
			answer: answer,
		},
		success: function(data) {

			var userCorrectClass = data.user_correct == 1 ? 'correct':'wrong';

			$('.loading').slideUp(350, function() {
				$('.answer.'+userCorrectClass).stop(true,true).slideDown(350, function() {
                    if (data.google_text != '')
					{
                        $('.googles-answer .text').html(data.google_text);
                    }
					$('.googles-answer').delay(350).stop(true,true).slideDown(350);
				});
			});

			// update scores
			$('.scores .you .score .correct').html(data.user_score);
			$('.scores .you .score .wrong').html(data.user_wrong);
			$('.scores .google .score .correct').html(data.google_score);
			$('.scores .google .score .wrong').html(data.google_wrong);

            if (data.last_question == 1)
            {
            	if(data.user_score > data.google_score) {
                	$('.share-results .smarter').show();
                } else if(data.user_score < data.google_score) {
                	$('.share-results .dumber').show();
                } else {
                	$('.share-results .draw').show();
                }
                $('.share-results').slideDown(350);

            }

		}

	});

});