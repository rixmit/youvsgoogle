<?php
session_start();

//error_reporting(E_ALL);
//ini_set('display_errors', '1');

while (true)
{
    // get some the quizzes
    $limit = 10000;
    $page = 0;
    $uri = "http://tools.ncrv.nl/nnqapi/index.php/quizzes/$limit/$page";
    $file = file_get_contents($uri);
    $json = json_decode($file); //print_r($json);

    if (count($json->result) == 0)
    {
        continue;
    }

    // pick a random quiz
    $n = rand(0, count($json->result)-1);
    $quiz = $json->result[$n];

    // pick a rando question
    $jsonq = json_decode(file_get_contents("http://tools.ncrv.nl/nnqapi/index.php/quiz/$quiz->id"));


    if (count($jsonq->result->questions) == 0)
    {
        continue;
    }
    $n = rand(0, count($jsonq->result->questions)-1);

    $question = $jsonq->result->questions[$n];

    break;

}

$id = $question->id;
$quizz_id = $question->quiz_id;
$title = $question->title;
$text = $question->text;
$answer_a = $question->answer_a;
$answer_b = $question->answer_b;
$answer_c = $question->answer_c;
$answer_d = $question->answer_d;
$correct_answer = $question->correct_answer;
$seconds = $question->seconds;
$points = $question->points;

// answer model
$answers = array(
    'a' => array('text' => $question->answer_a),
    'b' => array('text' => $question->answer_b),
    'c' => array('text' => $question->answer_c),
    'd' => array('text' => $question->answer_d)
);

$correctText = $answers[$correct_answer]['text'];

$_SESSION['question'] = $question;
$_SESSION['answers'] = $answers;

if (empty($_SESSION['started']) || isset($_GET['restart']) || empty($_SESSION['scoring']) || empty($_SESSION['question_number']))
{
    $scoring = array(
        'user' => array('correct' => 0, 'wrong' => 0),
        'google' => array('correct' => 0, 'wrong' => 0)
    );

    $_SESSION['started'] = 1;
    $questionNumber = 1;
    $_SESSION['question_number'] = $questionNumber;
    $_SESSION['scoring'] = $scoring;
    $endResult = false;
}
else
{
    $_SESSION['question_number']++;
    $questionNumber = $_SESSION['question_number'];
    
    if ($questionNumber == 10)
    {
        $endResult = true;
        $_SESSION['started'] = 0;           
    }
    else
    {
        $endResult = false;
    }

    $scoring = $_SESSION['scoring'];

}


?>


<!doctype html>
<html lang="en">
	
	<head>

		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

		<title>Ben jij slimmer dan Google?</title>
		<meta name="description" content="">
		<meta name="author" content="Jorrit Tinholt (dejorrit@gmail.com), Rik Smit (rik@rixmit.nl), Mark de Jonge">

		<meta name="viewport" content="initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,width=500,height=device-height,user-scalable=yes">

		<meta property="og:title" content="Ben jij slimmer dan Google?">
		<meta property="og:url" content="http://benjijslimmerdangoogle.nl">

		<link rel="stylesheet" href="/static/css/reset.css">
		<link rel="stylesheet" href="/static/css/style.css">
		<link rel="stylesheet" href="/static/css/buttons.css">

		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
		<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-45633074-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

	</head>

	<body>

		<div id="fb-root"></div>

		<div class="container">
			<div class="padder">
				

				<div class="logo">
					<img src="/static/img/logo.png">
				</div>

				<div class="page shadow">

					<div class="question">
						<p class="number">Vraag <?php echo $questionNumber; ?>/10</p>
						<p class="q"><?php echo $question->text; ?></p>
					</div>

					<div class="loading"><div class="chrome-load"></div></div>

					<div class="answers">
						<form method="post" action="" id="answer">
							<ul>
                                <?php foreach ($answers as $key => $answer)
                                {
    								echo '<li><label><input type="radio" name="answer" value="'.$key.'">'.$answer['text'].'</label></li>';
                                    
                                }
                                ?>
							</ul>
							<input type="submit" value="Geef antwoord" class="css-button orange">
						</form>
					</div>

					<div class="answer correct">
						<span class="strong">Goed geantwoord!</span>
					</div>

					<div class="answer wrong">
						<span class="strong">Helaas,</span> het goede antwoord was "<?php echo $correctText; ?>"
					</div>

					<div class="googles-answer">
						Google antwoordde "<span class="text strong">Ik heb geen idee...</span>" 
                        <?php if (!$endResult) { echo '<a href="/">Volgende vraag</a>'; } else { echo '<a href="/?restart=1">Probeer het opnieuw!</a>'; } ?>
					</div>

					<div class="share-results">
						<div class="smarter">
							<p>
								Gefeliciteerd! Je bent slimmer dan Google! En dat mag iedereen weten
							</p>
							<p>
								Deel dit geweldige resultaat!
							</p>
							<p>
								<a onclick="shareFacebook();"><img src="/static/img/share-facebook.png"></a>
								<a onclick="shareTwitter();"><img src="/static/img/share-twitter.png"></a>
							</p>
						</div>
						<div class="dumber">
							<p>Helaas, Google is slimmer.. volgende keer beter.</p>
							<p><a href="/?restart=1">Opnieuw proberen?</a>
						</div>
						<div class="draw">
							<p>Gelijkspel!</p>
							<p><a href="/?restart=1">Opnieuw proberen?</a>
						</div>
					</div>

					<div class="scores clearfix">
						<div class="you clearfix">
							<div class="identifier">Jij</div>
							<div class="score"><span class="correct"><?php echo $scoring['user']['correct']; ?></span> goed / <span class="wrong"><?php echo $scoring['user']['wrong']; ?></span> fout</div>
						</div>
						<div class="google clearfix">
							<div class="identifier">Google</div>
							<div class="score"><span class="correct"><?php echo $scoring['google']['correct']; ?></span> goed / <span class="wrong"><?php echo $scoring['google']['wrong']; ?></span> fout</div>
						</div>
					</div>

				</div>

				<p class="center light">Gemaakt tijdens <a target="_blank" href="http://www.hackdeoverheid.nl/hackathon-publieke-omroep-2/">#hack123</a>. Voor info contact <a target="_blank" href="http://twitter.com/dejorrit">@dejorrit</a></p>

			</div>
		</div>

		<script src="/static/js/init.js"></script>

		<script>

			window.fbAsyncInit = function() {
				
				FB.init({
					appId      : '232525350251866',
					channelUrl : '//benjijslimmerdangoogle.nl/channel.html',
					status     : true,
					xfbml      : false
				});

			};

			// Load the SDK asynchronously
			(function(d, s, id){
				var js, fjs = d.getElementsByTagName(s)[0];
				if (d.getElementById(id)) {return;}
				js = d.createElement(s); js.id = id;
				js.src = "//connect.facebook.net/en_US/all.js";
				fjs.parentNode.insertBefore(js, fjs);
			}(document, 'script', 'facebook-jssdk'));

		</script>


		<script>
		function shareFacebook() {
		var x = screen.width/2 - 626/2;
		var y = screen.height/2 - 436/2;
		window.open(
		'https://www.facebook.com/sharer/sharer.php?u='+encodeURIComponent('http://benjijslimmerdangoogle.nl'), 
		'facebook-share-dialog', 
		'width=626,height=436,left='+x+',top='+y);
		_gaq.push(['_trackEvent', 'Share Purchase', 'Facebook']);
		}
		function shareTwitter() {
		var x = screen.width/2 - 626/2;
		var y = screen.height/2 - 436/2;
		var text = 'Ik ben slimmer dan Google! Doe de test op http://benjijslimmerdangoogle.nl';
		window.open(
		'https://twitter.com/share/?text='+encodeURIComponent(text)+'&url=false', 
		'twitter-share-dialog', 
		'width=626,height=436,left='+x+',top='+y); 
		_gaq.push(['_trackEvent', 'Share Purchase', 'Twitter']);
		}
		</script>

	</body>

</html>